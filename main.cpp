#include <iostream>

using namespace std;

void test() {
    cout << "Test" << endl;
}

void num(int i) {
    cout << i << endl;
}

int main() {
    cout << "Hello, World!" << endl;

    void (*tp)();
    void (*np)(int);

    tp = test;

    (*tp)();
    tp();

    np = num;

    (*np)(3);
    np(3);

    return 0;
}
