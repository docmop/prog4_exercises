//
// Created by Philip Hoffmann on 12.06.19.
// Matr. Nr.: 3552659
//

#include "deque_test.h"

using namespace std;

template <class T>
void test_dqueue_t(vector<T> v) {
    // ein Element am Ende anfügen (push_back)
    // ein Element am Anfang einfügen (push_front)
    // das letzte Element entfernen (pop_back)
    // das erste Element entfernen (pop_front)
    // Konstruktion mit Hilfe eines Arrays von char-Strings
    // Kopierkonstrukor und Zuweisungsoperator,
    // == und != für inhaltliche Vergleiche zweier Deques
    // + und += für das Aneinanderhängen zweier Deques
    // [] für den elementweisen Zugriff
}

int main(int argc, char *argv[]) {
     vector<string> s = {"aaa"s, "bbb"s, "ccc"s, "ddd"s, "eee"s, "fff"s};
     vector<int> i = {1, 2, 3, 4, 5, 6, 7};
     vector<char> c = {'a', 'b', 'c', 'd', 'e', 'f'};

    test_dqueue_t(s);
    test_dqueue_t(i);
    test_dqueue_t(c);
    return 0;
}