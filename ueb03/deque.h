//
// Created by Philip Hoffmann on 12.06.19.
// Matr. Nr.: 3552659
//

#ifndef CODE_DEQUE_H
#define CODE_DEQUE_H


class deque {
    public:
        void push_back();
        void push_front();
        void pop_back();
        void pop_front();

        deque() = default;
};


#endif //CODE_DEQUE_H
