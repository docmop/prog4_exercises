//
// Created by Philip Hoffmann on 12.06.19.
// Matr. Nr.: 3552659
//

#ifndef CODE_DEQUE_TEST_H
#define CODE_DEQUE_TEST_H

#include <iostream>
#include <vector>
#include "deque.h"
#include "deque_t.h"

void test_dqueue_t();

#endif //CODE_DEQUE_TEST_H
