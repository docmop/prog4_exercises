//
// Created by Philip Hoffmann on 31.05.19.
// Matr. Nr.: 3552659
//

#ifndef CODE_STR_CMP_IGN_WS_H
#define CODE_STR_CMP_IGN_WS_H

#include <iostream>
#include <algorithm>

// Step to next non-whitespace character
const unsigned char* ign_ws(const unsigned char* s) {
    for(; *s != '\0' && isspace(*s); s++) {
    }
    return s;
}

// TODO: Use this instead of isspace
bool myisspace(const char* c) {
    switch(*c) {
        case ' ':
        case '\n':
        // ...
        case '\t':
            return true;
        default:
            return false;
    }
}

int myclamp(int i, int lower, int upper) {
    if(i < lower) {return lower;}
    else if(i > upper) {return upper;}
    else {return i;}
}

/**
 * Lexically compares strings, ignores whitespaces
 * @param s1 First string
 * @param s2 Second string
 * @return -1 fist string smaller, 0 strings match, 1 first string bigger
 */
int strcmp_ign_ws(const char* p1, const char* p2) {
    auto *s1 = (const unsigned char *) p1;
    auto *s2 = (const unsigned char *) p2;

    while(*s1 != '\0' && *s2 != '\0') {
        // Step to next non-whitespace character
        s1 = ign_ws(s1);
        s2 = ign_ws(s2);

        if(*s1 != *s2) {
            break;
        }

        s1++;
        s2++;
    }

    // Ignore trailing whitespaces
    s1 = ign_ws(s1);
    s2 = ign_ws(s2);

    return myclamp(*s1 - *s2, -1, 1);
}

#endif //CODE_STR_CMP_IGN_WS_H
