//
// Created by Philip Hoffmann on 31.05.19.
// Matr. Nr.: 3552659
//

#ifndef CODE_STR_WCOUNT_H
#define CODE_STR_WCOUNT_H

#include <cctype>

/**
 * Counts words separated by whitespaces
 * @param s String of words
 * @return wcount Word count
 */
int str_wcount(const char* s) {
    int wcount = 0;

    while(*s != '\0') {
        if(isspace(*s)) {
            // Read whitespace until non-whitespace
            for(; *s != '\0' && isspace(*s); s++) {
            }
        } else {
            // Read non-whitespace until whitespace
            for (; *s != '\0' && !isspace(*s); s++) {
            }
            wcount++;
        }
    }

    return wcount;
}

#endif //CODE_STR_WCOUNT_H
