//
// Created by Philip Hoffmann on 31.05.19.
// Matr. Nr.: 3552659
//

#include "str_cmp_ign_ws.h"
#include "str_wcount.h"
#include <iostream>

void test_str_wcount(char* s, int expected) {
    int wcount = str_wcount(s);

    if(wcount == expected) {
        std::cout << "OK: Found ";
    } else {
        std::cout << "NOK: Found ";
    }

    std::cout << str_wcount(s) << " words in '" << s << "', expected " << expected << std::endl;
}

void test_str_cmp_ign_ws(char* s1, char* s2, int expected) {
    int comparison = strcmp_ign_ws(s1, s2);

    if(comparison == expected) {
        std::cout << "OK: '";
    } else {
        std::cout << "NOK: '";
    }

    std::cout << s1  << "' <-> '" << s2 << "': " << comparison << " expected " << expected << std::endl;
}

int main(int argc, char *argv[]) {
    char s1[] = "Hello World!";
    char s2[] = "Hello";
    char s3[] = "";
    char s4[] = "   ";
    char s5[] = "trailing space ";
    char s6[] = " leading space ";
    char s7[] = " spaces everywhere ";
    char s8[] = "More words to count. Should be 7.";
    char s9[] = "These\tare\ttabs!";
    char s10[] = "Linefeed\nover\nfour\nlines";

    std::cout << "WORD COUNT TESTS:" << std::endl;
    test_str_wcount(s1, 2);
    test_str_wcount(s2, 1);
    test_str_wcount(s3, 0);
    test_str_wcount(s4, 0);
    test_str_wcount(s5, 2);
    test_str_wcount(s6, 2);
    test_str_wcount(s7, 2);
    test_str_wcount(s8, 7);
    test_str_wcount(s9, 3);
    test_str_wcount(s10, 4);

    std::cout << std::endl;
    std::cout << "COMPARISON TESTS:" << std::endl;

    char s11[] = "ab";
    char s12[] = "bb";
    char s13[] = "ad";
    char s14[] = "a b c";
    char s15[] = "abc";
    char s16[] = " ab c ";

    test_str_cmp_ign_ws(s11, s12, -1);
    test_str_cmp_ign_ws(s11, s11, 0);
    test_str_cmp_ign_ws(s12, s13, 1);
    test_str_cmp_ign_ws(s14, s15, 0);
    test_str_cmp_ign_ws(s15, s16, 0);
    test_str_cmp_ign_ws(s12, s16, 1);

    return 0;
}
