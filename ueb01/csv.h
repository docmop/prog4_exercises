/**
 * Liest und sortiert eine CSV Datei mit Studenten
 *
 * @file csv.cpp
 * @author Hoffmann (3552659)
 * @date 13.05.2019
 *
 */

#ifndef CODE_CSV_H
#define CODE_CSV_H

#include "student.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

void run(std::ifstream *infile);
bool decodeStudent(const std::string &line, Student &stud);
int readData(std::vector<Student> &tab, std::ifstream &infile);

bool sortAvg(const Student &a, const Student &b);
bool sortName(const Student &a, const Student &b);

#endif //CODE_CSV_H
