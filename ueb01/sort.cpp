/**
 * Sorting helpers for std::sort()
 *
 * @file csv.cpp
 * @author Hoffmann (3552659)
 * @date 13.05.2019
 *
 */

#include "csv.h"

using namespace std;

/**
 * Comparison helper for std::sort()
 * For sorting students by avg (ascending)
 *
 * @param a student A
 * @param b student B
 * @return
 */
bool sortAvg(const Student &a, const Student &b) {
    return a.avg < b.avg;
}

/**
 * Comparison helper for std::sort()
 * Sort students by surname and prename (lexically)
 *
 * @param a student A
 * @param b student B
 * @return
 */
bool sortName(const Student &a, const Student &b) {
    // Concatenate string to compare full surname lexically
    string a_fullname = a.surname + a.prename;
    string b_fullname = b.surname + b.prename;

    return a_fullname.compare(b_fullname) < 0;
}
