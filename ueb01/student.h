/**
 * Student structure
 *
 * @file csv.cpp
 * @author Hoffmann (3552659)
 * @date 13.05.2019
 *
 */

#ifndef CODE_STUDENT_H
#define CODE_STUDENT_H

#include <string>
#include <vector>
#include <iostream>

struct Student
{
    int matrNr;
    std::string surname;
    std::string prename;
    std::string date;  // TODO: Better date type
    float avg;

    std::string toString() const {
        return std::to_string(matrNr)
            + " - " + surname
            + " - " + prename
            + " - " + date
            + " - " + std::to_string(avg);
    }
};

inline std::ostream & operator<<(std::ostream & Str, Student const & stud) {
    return Str << stud.toString();
}

inline std::ostream & operator<<(std::ostream & Str, std::vector<Student> const & stud_tab) {
    for(const Student& stud : stud_tab) {
        Str << stud  << std::endl;
    }
    return Str;
}

#endif //CODE_STUDENT_H
