/**
 * Reads & sorts CSV Datei with Studenten
 *
 * @file csv.cpp
 * @author Hoffmann (3552659)
 * @date 13.05.2019
 *
 */

#include "csv.h"

using namespace std;

/**
 * Run
 * @param infile Input file stream
 */
void run(ifstream &infile) {
    vector<Student> stud_tab;

    // Read infile and fill stud_tab with extracted data
    int count = readData(stud_tab, infile);

    // Print students unsorted
    cout << "Read " << count << " students" << endl;
    cout << "\nUnsorted:" << endl;
    cout << stud_tab;

    // Sort + print students by average
    cout << "\nSorted (by avg):" << endl;
    sort(stud_tab.begin(), stud_tab.end(), sortAvg);
    cout << stud_tab;

    // Sort + print students by name
    cout << "\nSorted: (by surname + prename)" << endl;
    sort(stud_tab.begin(), stud_tab.end(), sortName);
    cout << stud_tab;
}

int main(int argc, char *argv[]) {
    if(argc > 1) {
        cout << "Reading file " << argv[1] << endl;
    } else {
        cerr << "Usage: csv_stud_reader [FILE]" << endl;
        return 1;
    }

    ifstream infile;
    infile.open(argv[1]);

    if (!infile) {
        cerr << "Could not open " << argv[1] << endl;
        return 2;
    }

    run(infile);

    return 0;
}
