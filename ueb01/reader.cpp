/**
 * Read & decode student csv file
 *
 * @file csv.cpp
 * @author Hoffmann (3552659)
 * @date 13.05.2019
 *
 */

#include "csv.h"

#define CELLS_PER_LINE 5
#define CELL_DELIMITER ';'

using namespace std;

/**
 * Decode line into student data
 *
 * @param line Sting that shall be parsed for Student data
 * @param stud Student pointer where extracted data is stored
 * @return Returns state of data integrity check
 */
bool decodeStudent(const string &line, Student &stud) {
    stringstream lineStream(line);
    string cell;
    vector<string> cells;
    int cellcount = 0;

    // Split line into data cells
    while(getline(lineStream, cell , CELL_DELIMITER)) {
        // Empty cells are illegal
        if(cell.length() == 0)
            return false;

        cells.push_back(cell);
        cellcount++;
    }

    // Line has either to many or to few of cells
    if(cellcount != CELLS_PER_LINE)
        return false;


    // Try to put data into Student
    try { stud.matrNr = stoi(cells.at(0)); }
    catch (const std::invalid_argument& e) { return false; }

    stud.surname = cells.at(1);
    stud.prename = cells.at(2);
    stud.date = cells.at(3);

    try { stud.avg = stof(cells.at(4)); }
    catch (const std::invalid_argument& e) { return false; }

    return true;
}

/**
 * Read csv data file
 *
 * @param tab Vector of students to store data to
 * @param infile Input file stream
 */
int readData(vector<Student> &tab, ifstream &infile) {
    int count = 0;
    string line;
    Student stud;

    // Read line by line
    while (getline(infile, line)) {

        // Only append legit data
        if(decodeStudent(line, stud)) {
            tab.push_back(stud);
            count++;
        }
    }

    return count;
}
